import { createMuiTheme } from '@material-ui/core/styles';
import { green, red, yellow } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#556cd6',
      stateUnchanged: yellow[700],
      statePositive: green[500],
      stateNegative: red[500],
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
