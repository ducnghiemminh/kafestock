import axios from 'axios';

import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import MainNav from '../src/MainNav';
import TickerTable from '../src/TickerTable';

const Index = () => {
  return (
    <React.Fragment>
      <MainNav />
      <Container>
        <Box my={4}>
          <TickerTable />
        </Box>
      </Container>
    </React.Fragment>
  );
}

// Index.getInitialProps = async ctx => {
//   try {
//     const res = await axios.get('https://finfo-api.vndirect.com.vn/v4/stock_prices?sort=date&q=code:MBB~date:gte:2021-01-18~date:lte:2021-01-18&size=15&page=1');
//     console.log({ res });
//     return [];
//   } catch (error) {
//     return { error };
//   }
// }

export default Index;
