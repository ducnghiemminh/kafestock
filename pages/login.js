import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

import LockIcon from '@material-ui/icons/Lock';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const credentials = {
  username: 'kafestock',
  password: 'wearedabest',
};

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 400,
    marginTop: '50%',
    padding: theme.spacing(4),
  },
  text: {
    textAlign: 'center',
  },
  input: {
    width: '100%',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  button: {
    marginTop: theme.spacing(4),
    width: '100%'
  },
}));

const Login = () => {
  const classes = useStyles();
  const router = useRouter();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const [message, setMessage] = useState({
    type: 'success',
    text: '',
    isOpen: false,
  });

  useEffect(() => {
    if (localStorage.getItem('isAuthed')) {
      router.push('/');
    }
  }, []);

  const handleChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = () => {
    validate();

    if (errors.username || errors.password) {
      return false;
    }

    if (username !== credentials.username || password !== credentials.password) {
      setMessage({
        isOpen: true,
        type: 'error',
        text: 'Wrong username or password'
      });
      return false;
    }

    // Do login
    localStorage.setItem('isAuthed', 1);
    setMessage({
      isOpen: true,
      type: 'success',
      text: 'Login successfully'
    });
    setTimeout(() => {
      router.push('/');
    }, 3000);
  };

  const validate = () => {
    const validationErrors = {};
    if (!username.length) {
      validationErrors['username'] = 'Please enter the username.';
    }
    if (!password.length) {
      validationErrors['password'] = 'Please enter the password.';
    }

    setErrors(validationErrors);
  };

  const isError = (name) => {
    return !!errors[name];
  };

  const getError = (name) => {
    return errors[name];
  };

  return (
    <React.Fragment>
      <Container>
        <Grid container justify="center">
          <Grid item>
            <Paper elevation={3} variant="outlined" className={classes.paper}>
              <Typography component="h1" variant="h4" className={classes.text}>
                Kafe Stock
              </Typography>
              <form className={classes.form} noValidate autoComplete="off">
                <TextField
                  label="Username"
                  value={username}
                  error={isError('username')}
                  onChange={handleChangeUsername}
                  className={classes.input}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AccountCircleIcon />
                      </InputAdornment>
                    ),
                  }}
                  helperText={isError('username') ? getError('username') : ''}
                ></TextField>
                <TextField
                  label="Password"
                  type="password"
                  value={password}
                  error={isError('password')}
                  onChange={handleChangePassword}
                  className={classes.input}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LockIcon />
                      </InputAdornment>
                    ),
                  }}
                  helperText={isError('password') ? getError('password') : ''}
                ></TextField>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={handleSubmit}
                >
                  Login
                </Button>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </Container>
      <Snackbar open={message.isOpen} autoHideDuration={3000}>
        <Alert severity={message.type}>
          {message.text}
        </Alert>
      </Snackbar>
    </React.Fragment>
  );
}

export default Login;
